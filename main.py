import pandas as pd
import glob, os, csv, re, math, itertools
from nltk import PorterStemmer


#
#CLASS FOR STORING THE SPAM COUNT AND HAM COUNT OF A GIVEN WORD IN VOCABULARY
#
class bayes_term():
    def __init__(self):
        self.term_spam = None
        self.term_ham = None

#GENERATES THE COUNT FOR EACH WORD IN THE GIVEN DATA-SET
def count_generator(class_doc):
    class_dict = {}
    for each in class_doc['Message']:
        word_l = each.split(' ')
        for word in word_l:
            if word not in class_dict:
                class_dict[word] = 0
            class_dict[word] += 1
    return class_dict

################################################################
#GENERATES MODIFIED FILE
def text_preProcessing2(temp_data, f_path):
    for f_name in glob.glob(os.path.join(f_path, '*.txt')):
        t_file = pd.read_csv(f_name, sep='\t', encoding="latin-1", quoting=csv.QUOTE_NONE, names=['Word'])
        inline = ' '.join(t_file.Word)
        inline = inline.lower()
        inline = inline.replace('_', '')
        list_of_words = re.sub("[^a-zA-Z0-9]", " ", inline).split()
        new_list = list(map(lambda x: PorterStemmer().stem(x), list_of_words))
        each_line = ' '.join(new_list)
        temp_data.append(each_line)
    return temp_data


def modify_file(path_spam, path_ham, path_modified):
    empty_data = []
    f_spam = text_preProcessing2(empty_data, path_spam)
    f_spam_ham = text_preProcessing2(f_spam, path_ham)
    f_data = pd.DataFrame(f_spam_ham)
    f_data.to_csv(path_or_buf=path_modified, sep=' ', index=False, header=False)

def text_preProcessing_stopwords(temp_data, f_path, spw):
    for f_name in glob.glob(os.path.join(f_path, '*.txt')):
        t_file = pd.read_csv(f_name, sep='\t', encoding="latin-1", quoting=csv.QUOTE_NONE, names=['Word'])
        inline = ' '.join(t_file.Word)
        inline = inline.lower()
        inline = inline.replace('_', '')
        list_of_words = re.sub("[^a-zA-Z0-9]", " ", inline).split()
        for term in spw:
            if term in list_of_words:
                list_of_words.remove(term)
        new_list = list(map(lambda x: PorterStemmer().stem(x), list_of_words))
        each_line = ' '.join(new_list)
        temp_data.append(each_line)
    return temp_data

def modify_file_stopwords(path_spam, path_ham, path_modified, spw):
    empty_data = []
    f_spam = text_preProcessing_stopwords(empty_data, path_spam, spw)
    f_spam_ham = text_preProcessing_stopwords(f_spam, path_ham, spw)
    f_data = pd.DataFrame(f_spam_ham)
    f_data.to_csv(path_or_buf=path_modified, sep=' ', index=False, header=False)

################################################################################
def attach_class_train(train):
    c_1 = []
    for _ in itertools.repeat(None, 123):
        c_1.append('Spam')
    for _ in itertools.repeat(None, 340):
        c_1.append('Ham')
    train['Class'] = c_1
    return train

def attach_class_test(test):
    c_2 = []
    for _ in itertools.repeat(None, 130):
        c_2.append('Spam')
    for _ in itertools.repeat(None, 348):
        c_2.append('Ham')
    test['Class'] = c_2
    return test



#
# MULTI-NOMIAL NAIVE BAYES ALGORITHM
#

def algorithm(train_df, test_df):
    class_labels = ['Spam', 'Ham']

    class_spam = train_df.loc[train_df['Class'] == 'Spam']
    spam_vocab = count_generator(class_spam)
    # print('Spam Vocab\n',spam_vocab['87'])

    class_ham = train_df.loc[train_df['Class'] == 'Ham']
    ham_vocab = count_generator(class_ham)
    # print('Ham vocab\n', ham_vocab['87'])

    global_vocab = count_generator(train_df)
    # print('Global vocab\n', global_vocab)

    # number of docs in dataSet
    N = train_df.shape[0]
    # print(N)
    prior = {}
    prior['Spam'] = len(train_df.loc[train_df['Class'] == 'Spam']) / N
    prior['Ham'] = len(train_df.loc[train_df['Class'] == 'Ham']) / N

    cond_probs = {}

    #
    # TRAIN ALGORITHM ON THE TRAINING DATA-SET
    #
    for label in class_labels:
        class_c = train_df.loc[train_df['Class'] == label]
        c_vocab = count_generator(class_c)
        for global_term in global_vocab:
            if global_term in cond_probs:
                if label is 'Spam':
                    if global_term not in c_vocab:
                        cond_probs[global_term].term_spam = 1 / (sum(c_vocab.values()) + len(global_vocab))
                    else:
                        cond_probs[global_term].term_spam = (c_vocab[global_term] + 1) / (
                        sum(c_vocab.values()) + len(global_vocab))
                if label is 'Ham':
                    if global_term not in c_vocab:
                        cond_probs[global_term].term_ham = 1 / (sum(c_vocab.values()) + len(global_vocab))
                    else:
                        cond_probs[global_term].term_ham = (c_vocab[global_term] + 1) / (
                        sum(c_vocab.values()) + len(global_vocab))
            else:
                temp_term = bayes_term()
                if label is 'Spam':
                    if global_term not in c_vocab:
                        temp_term.term_spam = 1 / (sum(c_vocab.values()) + len(global_vocab))
                    else:
                        temp_term.term_spam = (c_vocab[global_term] + 1) / (sum(c_vocab.values()) + len(global_vocab))
                if label is 'Ham':
                    if global_term not in c_vocab:
                        temp_term.term_ham = 1 / (sum(c_vocab.values()) + len(global_vocab))
                    else:
                        temp_term.term_ham = (c_vocab[global_term] + 1) / (sum(c_vocab.values()) + len(global_vocab))
                cond_probs[global_term] = temp_term

    #
    # APPLY MULTI-NOMIAL ALGORITHM NAIVE BAYES FOR TESTING
    #
    count_accuracy = 0
    new_class = []
    for record in test_df['Message']:
        words_in_record = record.split(' ')
        score = {}
        for each in class_labels:
            score[each] = math.log(prior[each], 2)
            for term in words_in_record:
                if each is 'Spam':
                    if term in cond_probs:
                        if cond_probs[term].term_spam is not None:
                            score[each] += math.log(cond_probs[term].term_spam, 2)
                    else:
                        score[each] += math.log(1 / (sum(spam_vocab.values()) + len(global_vocab)), 2)
                if each is 'Ham':
                    if term in cond_probs:
                        if cond_probs[term].term_ham is not None:
                            score[each] += math.log(cond_probs[term].term_ham, 2)
                    else:
                        score[each] += math.log(1 / (sum(ham_vocab.values()) + len(global_vocab)), 2)
        if score['Spam'] > score['Ham']:
            new_class.append('Spam')
        else:
            new_class.append('Ham')

    for k, v in enumerate(test_df['Class']):
        if new_class[k] == v:
            count_accuracy += 1
    print(count_accuracy / len(test_df), '\n')


#
# MAIN
#

#TRAIN DATA SET
'''
path_train_spam = 'D:\\machineLearning\\hw2\\train\\spam'
path_train_ham = 'D:\\machineLearning\\hw2\\train\\ham'

#TEST DATA SET

path_test_spam = 'D:\\machineLearning\\hw2\\test\\spam'
path_test_ham = 'D:\\machineLearning\\hw2\\test\\ham'

#MODIFIED FILES CREATION

modify_file(path_train_spam, path_train_ham, 'D:\\machineLearning\\hw2\\train_modified.txt')
modify_file(path_test_spam, path_test_ham, 'D:\\machineLearning\\hw2\\test_modified.txt')
'''
#READING FROM THE MODIFIED FILES
train_set = pd.read_csv('D:\\machineLearning\\hw2\\train_modified.txt', names=['Message'])
test_set = pd.read_csv('D:\\machineLearning\\hw2\\test_modified.txt', names=['Message'])

#TRAIN AND TEST ON THE ALGORITHM WITHOUT REMOVING THE STOP WORDS
train_df = attach_class_train(train_set)
test_df = attach_class_test(test_set)
print('ACCURACY WITHOUT REMOVING STOPWORDS: ', end='')
algorithm(train_df, test_df)

#
#TRAIN AND TEST ON THE ALGORITHM WITH REMOVING THE STOP WORDS
#
st = open("D:\\machineLearning\\hw2\\stopwords.txt").readlines()
stopwords = list(map(lambda x: x.replace('\n', ''), st))


#MODIFIED FILES CREATION
'''
modify_file_stopwords(path_train_spam, path_train_ham, 'D:\\machineLearning\\hw2\\train_modified_stopwords.txt', stopwords)
modify_file_stopwords(path_test_spam, path_test_ham, 'D:\\machineLearning\\hw2\\test_modified_stopwords.txt', stopwords)
'''
train_set_stop = pd.read_csv('D:\\machineLearning\\hw2\\train_modified_stopwords.txt', names=['Message'])
test_set_stop = pd.read_csv('D:\\machineLearning\\hw2\\test_modified_stopwords.txt', names=['Message'])

train_stop_df = attach_class_train(train_set_stop)
test_stop_df = attach_class_test(test_set_stop)
print('ACCURACY BY REMOVING STOPWORDS: ',end='')
algorithm(train_stop_df, test_stop_df)


