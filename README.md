##MACHINE LEARNING I IMPLEMENTATION AND TESTING OF THE MULTINOMIAL NAIVE BAYES ALGORITHM FOR TEXT CLASSIFICATION

##Project Description

Code Language 	: Python

Libraries Used	: Pandas, NLTK

Reference : http://nlp.stanford.edu/IR-book/pdf/13bayes.pdf  and http://www.ranks.nl/resources/stopwords.html

•	Implemented the multinomial Naive Bayes algorithm for text classiﬁcation. 

•	Add-one Laplace smoothing is used in the algorithm. 

•	In the dataset, punctuation and special characters are removed and  normalized the words by converting them to lower case and also the plural words to singular (i.e., “Here” and “here” are the same word, “pens” and “pen” are the same word). 

•	All the calculations in code are log-scale to avoid underﬂow. 

•	Training dataset is used to make the algorithm learn and test set is used to report the accuracies.

•	To further improve the Naive Bayes algorithm, ﬁltered out stop words such as “the” “of” and “for” from all the documents. 

Results:

•	The accuracy increases as compared to that of containing the stop-words as it might be that the stop-words in that list doesn’t bear any significance and the removal of insignificant words increases the information content of text files after the removal.
